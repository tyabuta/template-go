
build:
	ls ./cmd | xargs -I{} go build -o bin/{} ./cmd/{}

clean:
	rm -rf bin

install:
	go install ./cmd/hello

run: build
	./bin/hello

